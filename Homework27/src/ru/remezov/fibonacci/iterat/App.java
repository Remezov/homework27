package ru.remezov.fibonacci.iterat;

public class App {
    public static void main(String[] args) {
        // Enter a number from 1.
        start(15);

    }

    private static void start(int count) {
        if (count == 1 ) {
            System.out.print("0");
        }
        if (count == 2 ) {
            System.out.print("0 1");
        }
        if (count > 2) {
            System.out.print("0 1 ");
            int a1 = 0;
            int a2 = 1;
            for (int i = 3; i <= count; i++) {
                int ai = a1 + a2;
                System.out.print(ai +" ");
                a1 = a2;
                a2 = ai;
            }
        }
    }
}
