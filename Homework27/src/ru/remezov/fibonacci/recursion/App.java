package ru.remezov.fibonacci.recursion;

import java.util.List;

public class App {
    public static void main(String[] args) {

        // Enter a number from 1.
        start(30);
    }

    private static void start(int count) {
        if (count == 1 ) {
            System.out.print("0");
        }
        if (count == 2 ) {
            System.out.print("0 1");
        }
        if (count > 2) {
            System.out.print("0 1 ");
            fibonacci(0, 1, 3, count);
        }
    }

    private static void fibonacci(int a0, int a1, int i, int count) {
        int ai = a0 + a1;
        System.out.print(ai + " ");
        i++;
        a0 = a1;
        a1 = ai;
        if ((i-1) != count) {
            fibonacci(a0, a1, i, count);
        }
    }
}
